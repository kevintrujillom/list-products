<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="url-root" content="{{ url('/') }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Valley Groups</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link href="{{ asset('css/public.css') }}" rel="stylesheet"> 
    
    <script src="{{ asset('js/public.js') }}" defer></script>
</head>

<body>

    <div id="app">

        @include('partials.header')

        <div class="container-custom-message">
            <div class="messages">
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert" style="border-radius: 0; margin-bottom: 0;">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif

                @if(Session::has('message'))
                    <div class="alert alert-primary" role="alert" style="border-radius: 0; margin-bottom: 0;">
                        {{ Session::get('message') }}
                    </div>
                @endif

                @if(Session::has('message_success'))
                    <div class="alert alert-success" role="alert" style="border-radius: 0; margin-bottom: 0;">
                        {{ Session::get('message_success') }}
                    </div>
                @endif

                @if(Session::has('message_error'))
                    <div class="alert alert-danger" role="alert" style="border-radius: 0; margin-bottom: 0;">
                        {{ Session::get('message_error') }}
                    </div>
                @endif
            </div>
        </div>     

        <main>
            @yield('content')
        </main>

        @include('partials.footer')

    </div>

    @yield('scripts')

</body>
</html>
