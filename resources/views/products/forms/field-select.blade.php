<div class="form-group">

    @if(isset($label))
        <label
            @if(!isset($var_colons) || ( isset($var_colons) && $var_colons )) for="{{ $var }}" @endif
            @if(isset($var_colons) && $var_colons) :for="{{ $var }}" @endif
            class="col-form-label text-md-right">{{ __($label) }}</label>
    @endif

    <select

        @if(!isset($var_colons) || ( isset($var_colons) && $var_colons )) id="{{ $var }}" @endif
        @if(!isset($var_colons) || ( isset($var_colons) && $var_colons )) name="{{ $var }}" @endif

        @if(isset($var_colons) && $var_colons) :id="{{ $var }}" @endif
        @if(isset($var_colons) && $var_colons) :name="{{ $var }}" @endif

        @if(isset($vmodel)) v-model="{{ $vmodel }}" @endif
        class="custom-select @error($var) is-invalid @enderror">

        @if (isset($empty))
            <option value="">Select an option</option>
        @endif

        @foreach ($list as $listitem)
            @if(isset($value))
                <option class="custom" value="{{ $listitem->id }}" @if ($listitem->id == old($var) || $listitem->name == $value) selected @endif>
                    {{ $listitem->name }}
                </option>
            @else
                <option value="{{ $listitem->id }}">
                    {{ $listitem->name }}
                </option>
            @endif
        @endforeach

    </select>

    @error($var)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror

</div>