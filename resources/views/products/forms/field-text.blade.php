<div class="form-group">

    @if(isset($label))
        <label
            for="{{ $var }}"
            class="col-form-label text-md-right">{{ __($label) }}</label>
    @endif

    @if(isset($help_text))
        <div class="text-info">
            <small>{!! $help_text !!}</small>
        </div>
    @endif

    <input
        type="text"
        id="{{ $var }}"
        name="{{ $var }}"
        class="form-control @error($var) is-invalid @enderror"
        @if (isset($placeholder)) placeholder="{{ $placeholder }}" @endif
        @if (isset($vmodel)) v-model="{{ $vmodel }}" @endif
        @if (isset($value)) value="{{ $value }}" @endif
        @if (isset($required)) required @endif>

    @error($var)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror

</div>