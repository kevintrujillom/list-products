@extends('layout')

@section('content')

<div class="page-container-form card">

    <div class="row-title">
        <div class="title">Create Product</div>
        <a href="/" class="btn btn-primary">Back</a>
    </div>

    <form method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
        @csrf
        @include('products.form')
    </form>
      
</div>

@endsection