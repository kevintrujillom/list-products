<div class="row">
    <div class="col-xs-12 col-sm-6">

        @include('products.forms.field-text', array(
            'label' => 'Name',
            'var' => 'name',
            'value' =>  $product->name ?? old('name') ))

    </div>
    <div class="col-xs-12 col-sm-6">

        @include('products.forms.field-text', array(
            'label' => 'Reference',
            'var' => 'reference',
            'value' => $product->reference ?? old('reference') ))

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6">

        @include('products.forms.field-text', array(
            'label' => 'Description',
            'var' => 'description',
            'value' => $product->description ?? old('description') ))

    </div>
    <div class="col-xs-12 col-sm-6">

        @include('products.forms.field-text', array(
            'label' => 'Units',
            'var' => 'units',
            'value' =>  $product->units ?? old('units') ))

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6">

        @include('products.forms.field-select', array(
            'value' => $product->brand->name ?? old('brand_name'),
            'label' => 'Brand',
            'var' => 'brand_id',
            'list' => $brands, ))

    </div>
    <div class="col-xs-12 col-sm-6">
    </div>
</div>

<div class="form-group">
    <a class="btn btn-dark" href="/">{{ __('Back') }}</a>
    <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
</div>