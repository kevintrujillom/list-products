@extends('layout')

@section('content')

<div class="page-container card">

    <div class="row-title">
        <div class="title">Edit Product - {{ $product->name }}</div>
        <a href="/" class="btn btn-primary">Back</a>
    </div>

    <form method="POST" action="{{ route('product.update', $product->id) }}" enctype="multipart/form-data">
        @csrf
        @include('products.form')

        <input type="hidden" name="id" value="{{ $product->id }}">
    </form>
</div>

@endsection