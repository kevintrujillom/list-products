@extends('layout')

@section('content')

<div class="page-container card">

    <div class="row-title">
        <div class="title">List Products</div>
        <a href="/create" class="btn btn-primary">Create product</a>    
    </div>

    <div class="table-responsive">

        <table class="table__products table table-bordered table-hover table-elastic">

            <thead class="thead-light">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Reference</th>
                    <th>Description</th>
                    <th>Units</th>
                    <th>Brand</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                @foreach($products as $product)

                    <tr>
                        <td><span>ID:</span> {{ $product->id }}</td>
                        <td><span>Name: </span> {{ $product->name }}</td>
                        <td><span>Reference: </span> {{ $product->reference }}</td>
                        <td><span>Description: </span> {{ $product->description }}</td>
                        <td><span>Units: </span> {{ $product->units }}</td>
                        <td><span>Brand: </span> {{ $product->brand->name }}</td>

                        <td class="text-right">
                            <span>Actions: </span>
                            <a href="/edit/{{ $product->id }}" class="btn"><i class="fa fa-pencil"></i></a>
                            <a href="/delete/{{ $product->id }}" class="btn"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>

                @endforeach
            </tbody>

        </table>

        <div class="paginate">
            {{ $products->links('vendor.pagination.bootstrap-4') }}
        </div>

    </div>

</div>

@endsection