<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ApiBrandsController;
use App\Http\Controllers\ApiProductsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * API URL for products table.
 */
Route::get('/products', [ApiProductsController::class, 'index']);
Route::get('/products/{id}', [ApiProductsController::class, 'show']);
Route::get('/products/delete/{id}', [ApiProductsController::class, 'destroy']);
Route::post('products/store', [ApiProductsController::class, 'store']);
Route::post('products/update/{id}', [ApiProductsController::class, 'update']);

/**
 * API URL for brands table.
 */
Route::get('/brands', [ApiBrandsController::class, 'index']);
Route::get('/brands/{id}', [ApiBrandsController::class, 'show']);
Route::get('/brands/delete/{id}', [ApiBrandsController::class, 'destroy']);
Route::post('brands/store', [ApiBrandsController::class, 'store']);
Route::post('brands/update/{id}', [ApiBrandsController::class, 'update']);
