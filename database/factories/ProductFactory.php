<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $brand_number = rand(1, 5);

        return [
            'name'        => $this->faker->name,
            'reference'   => $this->faker->unique()->text(10),
            'description' => $this->faker->text(20),
            'units'       => $this->faker->randomNumber(2),
            'brand_id'    => $brand_number,
        ];
    }
}
