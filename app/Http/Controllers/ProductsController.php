<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Brand;
use App\Models\Product;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')
                    ->paginate(5);
        
        return view('products.index')->with([
            'products'   => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::all();

        return view('products.create')->with([
            'brands'   => $brands,
            'empty' => true,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        return $this->saveOrUpdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = Brand::all();
        $product = Product::find($id);
        
        return view('products.edit')->with([
            'brands' => $brands,
            'product'   => $product,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        return $this->saveOrUpdate($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product) {

            $product->delete();

            $products = Product::all();

            return redirect()
                ->route('product.index')
                ->with([ 
                    'products' => $products,
                    'message' => "Product {$product->name}, removed successfully."
                ]);   

        } else {

            $products = Product::all();

            return redirect()
                ->route('product.index')
                ->with([
                    'products' => $products,
                    'message' => "Product not found, try again."
                ]);             
        }

    }

    private function saveOrUpdate(Request $request, $id = null)
    {
        $product = Product::find($id);
        
        if ($product) {

            $product->update([
                'name' => $request->input('name'),
                'reference' => $request->input('reference'),
                'description' => $request->input('description'),
                'units' => $request->input('units'),
                'brand_id' => $request->input('brand_id'),
            ]);

        } else {

            $product = Product::create([
                'name' => $request->input('name'),
                'reference' => $request->input('reference'),
                'description' => $request->input('description'),
                'units' => $request->input('units'),
                'brand_id' => $request->input('brand_id'),
            ]);
        }

        return redirect()
            ->route('product.edit', [ 'id' => $product->id ])
            ->with([ 'message' => "Product {$product->name}, saved successfully." ]);
    }
}
