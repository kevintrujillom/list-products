<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;

class ApiProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::All();
        
        if ($products) {

            return response()->json([ 
                'status' => 200,
                'response' => $products
            ], 200);

        }

        return response()->json([ 
            'status' => 404,
            'response' => 'No results'
        ], 404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('name') && $request->input('reference') &&
            $request->input('description') && $request->input('units') &&
            $request->input('brand_id')) {

            $product = Product::create([
                'name' => $request->input('name'),
                'reference' => $request->input('reference'),
                'description' => $request->input('description'),
                'units' => $request->input('units'),
                'brand_id' => $request->input('brand_id'),
            ]);
            
            if ($product) {
    
                return response()->json([ 
                    'status' => 200,
                    'response' => "Product {$product->name}, saved successfully."
                ], 200);
                
            } else {
    
                return response()->json([ 
                    'status' => 400,
                    'response' => "Product {$brand->name}, not saved."
                ], 400);              
            }

        } else {

            return response()->json([ 
                'status' => 400,
                'response' => "Some field empty."
            ], 400); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if ($product) {

            return response()->json([ 
                'status' => 200,
                'response' => $product
            ], 200);

        }

        return response()->json([ 
            'status' => 404,
            'response' => 'No results'
        ], 404);        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        if ($product) {

            if ($request->input('name') && $request->input('reference') &&
                $request->input('description') && $request->input('units') &&
                $request->input('brand_id')) {
    
                $product->update([
                    'name' => $request->input('name'),
                    'reference' => $request->input('reference'),
                    'description' => $request->input('description'),
                    'units' => $request->input('units'),
                    'brand_id' => $request->input('brand_id'),
                ]);
                
                if ($product) {
        
                    return response()->json([ 
                        'status' => 200,
                        'response' => "Product {$product->name}, updated successfully."
                    ], 200);
                    
                } else {
        
                    return response()->json([ 
                        'status' => 400,
                        'response' => "Product {$product->name}, not updated."
                    ], 400);              
                }
    
            } else {
    
                return response()->json([ 
                    'status' => 400,
                    'response' => "Some field empty."
                ], 400); 
            }

        }

        return response()->json([ 
            'status' => 400,
            'response' => "Product with id {$id}, not found."
        ], 400);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product) {

            $product->delete();

            return response()->json([ 
                'status' => 200,
                'response' => "Product {$product->name}, removed successfully."
            ], 200);

        }

        return response()->json([ 
            'status' => 404,
            'response' => "Product with id {$id}, not found."
        ], 404);         
    }
}
