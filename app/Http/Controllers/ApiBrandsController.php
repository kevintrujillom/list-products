<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Brand;
use App\Models\Product;

class ApiBrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Brands = Brand::All();
        
        if ($Brands) {

            return response()->json([ 
                'status' => 200,
                'response' => $Brands
            ], 200);

        }

        return response()->json([ 
            'status' => 404,
            'response' => 'No results'
        ], 404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('name')) {

            $brand = Brand::create([
                'name' => $request->input('name'),
            ]);
            
            if ($brand) {
    
                return response()->json([ 
                    'status' => 200,
                    'response' => "Brand {$brand->name}, saved successfully."
                ], 200);
                
            } else {
    
                return response()->json([ 
                    'status' => 400,
                    'response' => "Brand {$brand->name}, not saved."
                ], 400);              
            }

        } else {

            return response()->json([ 
                'status' => 400,
                'response' => "Field 'name' empty."
            ], 400); 
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::find($id);

        if ($brand) {

            return response()->json([ 
                'status' => 200,
                'response' => $brand
            ], 200);

        }

        return response()->json([ 
            'status' => 404,
            'response' => 'No results'
        ], 404);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::find($id);

        if ($brand) {

            if ($request->input('name')) {
    
                $brand->update([
                    'name' => $request->input('name'),
                ]);
                
                if ($brand) {
        
                    return response()->json([ 
                        'status' => 200,
                        'response' => "Brand {$brand->name}, updated successfully."
                    ], 200);
                    
                } else {
        
                    return response()->json([ 
                        'status' => 400,
                        'response' => "Brand {$brand->name}, not updated."
                    ], 400);              
                }
    
            } else {
    
                return response()->json([ 
                    'status' => 400,
                    'response' => "Field 'name' empty."
                ], 400); 
            }

        }

        return response()->json([ 
            'status' => 400,
            'response' => "Brand with id {$id}, not found."
        ], 400);               

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);

        if ($brand) {

            $products = Product::where('brand_id', '=', $id)->delete();
            
            $brand->delete();

            return response()->json([ 
                'status' => 200,
                'response' => "Brand {$brand->name}, removed successfully."
            ], 200);

        }

        return response()->json([ 
            'status' => 404,
            'response' => "Brand with id {$id}, not found."
        ], 404);   
    }
}
