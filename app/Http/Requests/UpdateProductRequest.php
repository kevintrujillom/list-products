<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $list = [
            'name' => 'required',
            'reference' => [
                'required',
                Rule::unique('products')->ignore(Request::get('id')),
            ],
            'description' => 'required',
            'units' => 'required|numeric',
            'brand_id' => 'required',
        ];
        
        return $list;
    }

    public function messages()
    {
        return [
            'name.required' => 'Please fill this field',
            'reference.required' => 'Please fill this field',
            'reference.unique' => 'This reference already exists in the database',
            'description.required' => 'Please fill this field',
            'units.required' => 'Please fill this field',
            'units.numeric' => 'This field must contain only numeric characters',
            'brand_id.required' => 'Please fill this field',
       ];
    }
}
