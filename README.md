# List Product

## Informaciòn del proyecto
```
Laravel: ^8.12
php: ^7.3 | ^8.0,

DB: mySQL
```

## Instalar dependencias
Una vez descargado el proyecto ejecutar los siguientes comandos:
```
npm install
composer install
```

## Base de datos
Para cargar datos de prueba ejecutar:
```
php artisan migrate:refresh --seed
```

## Iniciar servidor
Para iniciar el servidor ejecutar:
```
php artisan serve
```
En el navegador de preferencia ingresar el enlace para ver el proyecto:
```
http://127.0.0.1:8000
```

# API
Para consumir el API el servidor debe estar iniciado. Se recomienda el uso de POSTMAN o alguna herramienta similar como cliente.

1. Ejemplos de las urls para consumir los datos de la tabla 'products' de la base de datos
```
GET:    http://127.0.0.1:8000/api/products
GET:    http://127.0.0.1:8000/api/products/1
DELETE: http://127.0.0.1:8000/api/products/delete/5
POST:   http://127.0.0.1:8000/api/products/store
PUT:    http://127.0.0.1:8000/api/products/update/1


* Importante tener en cuenta que para consumir con exito los metodos POST Y PUT se deben enviar en la peticiòn los siguientes campos (como si se enviaràn desde un formulario) que corresponden al modelo 'product':

- _token: YoecyzdKUszkLcXNxGPsa3jA34fYYaD8BF8tvPih
- name: Camiseta
- reference: camiseta-123
- description: description
- units: 25
- brand_id: 2

(Este token es 'quemado' el verdadero es generado en los formulario en laravel) 

```

2. Ejemplos de las urls para consumir los datos de la tabla 'brands' de la base de datos
```
GET:    http://127.0.0.1:8000/api/brands
GET:    http://127.0.0.1:8000/api/brands/1
DELETE: http://127.0.0.1:8000/api/brands/delete/5
POST:   http://127.0.0.1:8000/api/brands/store
PUT:    http://127.0.0.1:8000/api/brands/update/1


* Importante tener en cuenta que para consumir con exito los metodos POST Y PUT se deben enviar en la peticiòn los siguientes campos (como si se enviaràn desde un formulario) que corresponden al modelo 'brand':

- _token: YoecyzdKUszkLcXNxGPsa3jA34fYYaD8BF8tvPih
- name: Adidas

(Este token es 'quemado' el verdadero es generado en los formulario en laravel) 
```

